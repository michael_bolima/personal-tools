# Hey there o/! 
# 
# We just wanted to let you know that we care a great deal about    
# making our git history clean, maintainable and easy to access for 
# all our contributors. Commit messages are very important to us,  
# which is why we have a strict commit message policy in place.     
# Please use the following guidelines to format all your commit     
# messages:
# 
#     <type>(<scope>): <subject>
#     <BLANK LINE>
#     <body>
#     <BLANK LINE>
#     <footer>
# 
# Please note that:
#  - The HEADER is a single line of max. 50 characters that         
#    contains a succinct description of the change. It contains a   
#    type, an optional scope, and a subject
#       + <type> describes the kind of change that this commit is   
#                providing. Allowed types are:
#             * feat (feature)
#             * fix (bug fix)
#             * docs (documentation e.g. READEME)
#             * style (formatting, missing semicolons, …)
#             * refactor (code change that neither fixes a bug nor adds a feature)
#             * test (when adding missing tests)
#             * chore (maintain)
#             * build (dependency)
#             * revert (commit hash)
#             * perf (code change that improves performance)
#       + <scope> can be anything specifying the place of the commit    
#                 change
#       + <subject> is a very short description of the change, in   
#                   the following format:
#             * imperative, present tense: “change” not             
#               “changed”/“changes”
#             * no capitalized first letter
#             * no dot (.) at the end
#
#     for example: feat(header): a header module for global use 
#
#  - The BODY should include the motivation for the change and      
#    contrast this with previous behavior and must be phrased in   
#    imperative present tense 
#  - The FOOTER should contain any information about Breaking       
#    Changes and is also the place to reference ticket issues that   
#    this commit closes
